# CashMachine

In order to run this app use .Net CLI (https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x):
Go to `~/CashMachine` and type:
`dotnet run`
and then browse specified host.
In order to run in release mode, go to `~/CashMachine` and type:
`dotnet publish -c Release`
then go to `~/CashMachine/bin/Release/netcoreapp2.0/publish`, type:
`dotnet CashMachine.dll`
and browse specified host.