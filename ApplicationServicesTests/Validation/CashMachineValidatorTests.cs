﻿using System;
using System.Collections.Generic;
using ApplicationServices.Exceptions;
using ApplicationServices.Validation;
using FluentAssertions;
using NUnit.Framework;

namespace ApplicationServicesTests.Validation
{
    [TestFixture]
    public class CashMachineValidatorTests
    {
        [Test]
        public void Should_Validate_Throw_InvalidArgumentException_When_RequestedAmount_LessThen_Zero()
        {
            // ARRANGE
            var denominations = new List<int>() { 10, 20, 50 };
            var requestedAmount = -90;

            // ACT
            Action act = () => _sut.Validate(requestedAmount, denominations);

            // ASSERT
            act.Should().ThrowExactly<InvalidArgumentException>()
                .WithMessage("Requested amount is negative");
        }

        [Test]
        public void Should_Validate_Throw_NoteUnavailableException_When_RequestedAmount_LessThan_Min_Available_Denomination()
        {
            // ARRANGE
            var denominations = new List<int>() { 10, 20, 50 };
            var requestedAmount = 7;

            // ACT
            Action act = () => _sut.Validate(requestedAmount, denominations);

            // ASSERT
            act.Should().ThrowExactly<NoteUnavailableException>()
                .WithMessage("Requested amount is less than minimal available denomination");
        }

        [Test]
        public void Should_Validate_Throw_NoteUnavailableException_When_RequestedAmount_Impossible_ToRetrieve()
        {
            // ARRANGE
            var denominations = new List<int>(){ 10, 20, 50 };
            var requestedAmount = 15;

            // ACT
            Action act = () => _sut.Validate(requestedAmount, denominations);

            // ASSERT
            act.Should().ThrowExactly<NoteUnavailableException>()
                .WithMessage("Requested amount is impossible to retrieve with available denominations");
        }

        [Test]
        public void Should_Not_Validate_Throw_When_RequestedAmount_Possible_ToRetrieve()
        {
            // ARRANGE
            var denominations = new List<int>() { 2, 5, 10 };
            var requestedAmount = 22;

            // ACT
            Action act = () => _sut.Validate(requestedAmount, denominations);

            // ASSERT
            act.Should().NotThrow();
        }

        [Test]
        public void Should_Validate_Throw_NoteUnavailableException_When_No_Available_Denominations()
        {
            // ARRANGE
            var requestedAmount = 20;

            // ACT
            Action act = () => _sut.Validate(requestedAmount, new List<int>());

            // ASSERT
            act.Should().ThrowExactly<NoteUnavailableException>()
                .WithMessage("No banknotes in the cash machine");
        }

        [SetUp]
        public void Setup()
        {
            _sut = new CashMachineValidator();
        }
        private CashMachineValidator _sut;
    }
}
