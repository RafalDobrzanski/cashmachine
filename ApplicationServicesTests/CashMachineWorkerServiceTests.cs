using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationServices;
using ApplicationServices.Abstractions;
using ApplicationServices.Validation;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace ApplicationServicesTests
{
    [TestFixture]
    public class CashMachineWorkerServiceTests
    {
        [Test]
        public void Should_GetNotes_ProvideProperResults_When_Amount_DifferentThen_DenominationValue()
        {
            // ARRANGE
            var requestedAmount = 60;

            // ACT
            var result = _sut.GetNotes(requestedAmount);

            // ASSERT
            result.Should().HaveCount(2).And.ContainSingle(x => x == 50).And.ContainSingle(x => x == 10);
        }

        [Test]
        public void Should_GetNotes_ProvideProperResults_When_NeedTo_Return_MultipleNotes_With_TheSame_Denomination()
        {
            // ARRANGE
            var requestedAmount = 280;

            // ACT
            var result = _sut.GetNotes(requestedAmount);

            // ASSERT
            result.Should().HaveCount(5).And
                .ContainSingle(x => x == 20).And
                .ContainSingle(x => x == 10).And
                .ContainSingle(x => x == 50).And
                .Subject.Where(x => x == 100).Should().HaveCount(2);
        }

        [Test]
        public void Should_GetNotes_ProvideProperResults_When_Amount_Equal_To_Denomination()
        {
            // ARRANGE
            var requestedAmount = 20;

            // ACT
            var result = _sut.GetNotes(requestedAmount);

            // ASSERT
            result.Should().OnlyContain(x => x == 20);
        }

        [Test]
        public void Should_GetNotes_ProvideProperResults_When_Amount_EqualTo_DenominationValue()
        {
            // ARRANGE
            var requestedAmount = 60;

            // ACT
            var result = _sut.GetNotes(requestedAmount);

            // ASSERT
            result.Should().HaveCount(2).And.ContainSingle(x => x == 50).And.ContainSingle(x => x == 10);
        }

        [SetUp]
        public void Setup()
        {
            _provider = new Mock<IDenominationsProvider>();
            _provider.Setup(x => x.GetAvailableDenominations()).Returns(Denominations);

            _sut = new CashMachineWorkerService(_provider.Object, Mock.Of<ICashMachineValidator>());
        }

        private Mock<IDenominationsProvider> _provider;
        private CashMachineWorkerService _sut;

        private const string CurrencyCode = "USD";
        private readonly IReadOnlyCollection<int> Denominations = new List<int>() {10, 20, 50, 100};
    }
}
