﻿using System;
using System.Collections.Generic;
using ApplicationServices;
using ApplicationServices.Abstractions;
using Microsoft.Extensions.Configuration;

namespace SomeDataProvider
{
    public class DenominationsProvider : IDenominationsProvider
    {
        private const string ConfigKey = "AvailableDenominations";
        private readonly IConfiguration _configuration;

        public DenominationsProvider(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public IReadOnlyCollection<int> GetAvailableDenominations() =>
            _configuration.GetSection(ConfigKey).Get<int[]>() ?? Array.Empty<int>();
    }
}