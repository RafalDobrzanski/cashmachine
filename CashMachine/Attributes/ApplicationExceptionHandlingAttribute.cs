﻿using CashMachine.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using ApplicationException = ApplicationServices.Exceptions.ApplicationException;

namespace CashMachine.Attributes
{
    public class ApplicationExceptionHandlingAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is ApplicationException)
            {
                context.Result = new PartialViewResult()
                {
                    ViewName = "_Error",
                    ViewData = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                    {
                        Model = new ErrorViewModel()
                        {
                            Name = context.Exception.GetType().Name,
                            Message = context.Exception.Message
                        }
                    }
                };
                context.ExceptionHandled = true;
            }
        }
    }
}
