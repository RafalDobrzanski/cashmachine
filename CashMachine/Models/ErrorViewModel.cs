using System;

namespace CashMachine.Models
{
    public class ErrorViewModel
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}