﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApplicationServices;
using CashMachine.Attributes;
using Microsoft.AspNetCore.Mvc;
using CashMachine.Models;

namespace CashMachine.Controllers
{
    [ApplicationExceptionHandling]
    public class HomeController : Controller
    {
        private readonly ICashMachineWorkerService _workerService;

        public HomeController(ICashMachineWorkerService workerService)
        {
            _workerService = workerService ?? throw new ArgumentNullException(nameof(workerService));
        }

        [HttpGet]
        public IActionResult Index() => View();

        [HttpPost]
        public IActionResult GetNotes(int requestedAmount) =>
            PartialView("_noteListResults", _workerService.GetNotes(requestedAmount));
    }
}
