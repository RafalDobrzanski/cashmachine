﻿namespace ApplicationServices.Exceptions
{
    public class InvalidArgumentException : ApplicationException
    {
        public InvalidArgumentException(string message) 
            : base(message)
        {}
    }
}