﻿using System;

namespace ApplicationServices.Exceptions
{
    public class NoteUnavailableException : ApplicationException
    {
        public NoteUnavailableException(string message) 
            : base(message)
        {}
    }
}