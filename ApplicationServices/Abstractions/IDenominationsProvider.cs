﻿using System.Collections.Generic;

namespace ApplicationServices.Abstractions
{
    public interface IDenominationsProvider
    {
        IReadOnlyCollection<int> GetAvailableDenominations();
    }
}