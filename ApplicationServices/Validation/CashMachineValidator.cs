﻿using System.Collections.Generic;
using System.Linq;
using ApplicationServices.Exceptions;

namespace ApplicationServices.Validation
{
    public class CashMachineValidator : ICashMachineValidator
    {
        public void Validate(int requestedAmount, IReadOnlyCollection<int> availableDenominations)
        {
            IsDenominationCollectionEmpty(availableDenominations);

            IsRequestedAmountNegative(requestedAmount);

            IsRequestedAmountLessThanMinAvailableDenomination(requestedAmount, availableDenominations);

            IsRequestedAmountImpossibleToRetrieveWithAvailableDenominations(requestedAmount, availableDenominations);
        }

        private void IsDenominationCollectionEmpty(IReadOnlyCollection<int> availableDenominations)
        {
            if (availableDenominations.Any() == false)
            {
                throw new NoteUnavailableException("No banknotes in the cash machine");
            }
        }

        private void IsRequestedAmountNegative(int requestedAmount)
        {
            if (requestedAmount < 0)
            {
                throw new InvalidArgumentException("Requested amount is negative");
            }
        }

        private void IsRequestedAmountLessThanMinAvailableDenomination(int requestedAmount,
            IReadOnlyCollection<int> availableDenominations)
        {
            if (requestedAmount < availableDenominations.Min())
            {
                throw new NoteUnavailableException("Requested amount is less than minimal available denomination");
            }
        }

        private void IsRequestedAmountImpossibleToRetrieveWithAvailableDenominations(int requestedAmount,
            IReadOnlyCollection<int> availableDenominations)
        {
            if (availableDenominations.All(x => requestedAmount % x != 0))
            {
                throw new NoteUnavailableException("Requested amount is impossible to retrieve with available denominations");
            }
        }
    }
}