﻿using System.Collections.Generic;

namespace ApplicationServices.Validation
{
    public interface ICashMachineValidator
    {
        void Validate(int requestedAmount, IReadOnlyCollection<int> availableDenominations);
    }
}