﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationServices.Abstractions;
using ApplicationServices.Validation;

namespace ApplicationServices
{
    public interface ICashMachineWorkerService
    {
        IList<int> GetNotes(int requestedAmount);
    }

    public class CashMachineWorkerService : ICashMachineWorkerService
    {
        private readonly IDenominationsProvider _denominationsProvider;
        private readonly ICashMachineValidator _validator;

        public CashMachineWorkerService(IDenominationsProvider denominationsProvider, ICashMachineValidator validator)
        {
            _denominationsProvider = denominationsProvider ?? throw new ArgumentNullException(nameof(denominationsProvider));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
        }

        public IList<int> GetNotes(int requestedAmount)
        {
            var denominations = _denominationsProvider.GetAvailableDenominations();

            _validator.Validate(requestedAmount, denominations);

            return GetNotesAgainstDenominations(denominations, requestedAmount);
        }

        private IList<int> GetNotesAgainstDenominations(IReadOnlyCollection<int> denominations, int requestedAmount)
        {
            var denominationItem = denominations.Where(x => x != 0 && x <= requestedAmount).Max();
            var division = Math.DivRem(requestedAmount, denominationItem, out var remainder);

            var result = new List<int>();
            result.AddRange(Enumerable.Repeat(denominationItem, division)
                .Concat(remainder != 0
                    ? GetNotesAgainstDenominations(denominations, remainder)
                    : Enumerable.Empty<int>()));

            return result;
        }
    }
}
